﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TesteAutoGlass.Infraestrutura.Utilidades.Contract
{
    public interface IValidation
    {
        /// <summary>
        /// Método que valida um CNPJ
        /// </summary>
        /// <param name="cnpj">CNPJ</param>
        /// <returns>Válido: true, Inválido: false</returns>
        bool CNPNValido(string cnpj);
    }
}
