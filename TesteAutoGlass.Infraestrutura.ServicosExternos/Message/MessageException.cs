﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TesteAutoGlass.Infraestrutura.Utilidades.Message
{
    public static class AppMessage
    {        
        #region Produto

        public static string CNPJ_INVALIDO = "CNPJ inválido.";
        public static string DT_FABRICACAO_MAIOR_DT_VALIDADE = "Data de Fabricação maior que a Data de Validade.";
        public static string PRODUTO_ATUALIZADO = "Produto atualizado com sucesso.";
        public static string PRODUTO_EXCLUIDO = "Produto excluído com sucesso.";
        public static string PRODUTO_NAO_ENCONTRADO = "Nenhum registro de produto foi encontrado.";
        public static string PRODUTO_JA_INATIVADO = "Produto já inativado.";

        #endregion
    }
}
