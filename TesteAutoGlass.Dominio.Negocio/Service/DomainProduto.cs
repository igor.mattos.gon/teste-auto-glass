﻿using TesteAutoGlass.Dominio.Entidade.Model;
using TesteAutoGlass.Dominio.Negocio.Contract;
using TesteAutoGlass.Infraestrutura.RepositorioEF.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TesteAutoGlass.Infraestrutura.Utilidades.Contract;
using TesteAutoGlass.Infraestrutura.Utilidades.Message;

namespace TesteAutoGlass.Dominio.Negocio.Service
{
    public class DomainProduto : DomainBase<Produto>, IDomainProduto
    {
        private readonly IRepositoryProduto _repositoryProduto;
        private readonly IValidation _validation;

        public DomainProduto(IRepositoryProduto repositoryProduto, IValidation validation)
            : base(repositoryProduto)
        {
            _repositoryProduto = repositoryProduto;
            _validation = validation;
        }

        /// <summary>
        /// Cadastrar um Produto
        /// </summary>
        /// <param name="produto">Objeto produto</param>
        /// <returns></returns>
        /// <exception cref="Exception">Exceção, caso exista</exception>
        public override async Task Cadastrar(Produto produto)
        {
            try
            {
                #region Validações

                ValidarCampos(produto);

                #endregion

                await base.Cadastrar(produto);
            }

            catch(Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Cadastrar um Produto
        /// </summary>
        /// <param name="produto">Objeto produto</param>
        /// <returns></returns>
        /// <exception cref="Exception">Exceção, caso exista</exception>
        public override async Task Atualizar(Produto produto)
        {
            try
            {
                #region Validações

                ValidarCampos(produto);

                Produto produtoAtualizacao = await _repositoryProduto.FindById(produto.IdProduto);

                if (produtoAtualizacao == null || (produtoAtualizacao != null && !produtoAtualizacao.Ativo))
                {
                    throw new Exception(AppMessage.PRODUTO_NAO_ENCONTRADO);
                }

                #endregion
                               
                await base.Atualizar(produto);
            }

            catch(Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Exclui um produto
        /// </summary>
        /// <param name="idProduto">Id do Produto</param>
        public override void Excluir(int idProduto)
        {
            try
            {
                Produto produtoExclusao = _repositoryProduto.FindById(idProduto).Result;

                if (produtoExclusao == null)
                {
                    throw new Exception(AppMessage.PRODUTO_NAO_ENCONTRADO);
                }

                else if (!produtoExclusao.Ativo)
                {
                    throw new Exception(AppMessage.PRODUTO_JA_INATIVADO);
                }

                else
                {
                    produtoExclusao.InativarProduto();
                    _repositoryProduto.Update(produtoExclusao).Wait();
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Método que valida os campos do Produto, retornando as exceções caso ocorram
        /// </summary>
        /// <param name="produto>Objeto Produto</param>
        private void ValidarCampos(Produto produto)
        {
            if (!String.IsNullOrEmpty(produto.CNPJFornecedor) && !_validation.CNPNValido(produto.CNPJFornecedor))
            {
                throw new Exception(AppMessage.CNPJ_INVALIDO);
            }

            if (produto.DataFabricacao > produto.DataValidade)
            {
                throw new Exception(AppMessage.DT_FABRICACAO_MAIOR_DT_VALIDADE);
            }
        }
    }
}
