﻿using TesteAutoGlass.Dominio.Negocio.Contract;
using TesteAutoGlass.Infraestrutura.RepositorioEF.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TesteAutoGlass.Dominio.Negocio.Service
{
    public class DomainBase<TEntity> : IDomainBase<TEntity> where TEntity : class
    {
        private readonly IRepositoryBase<TEntity> _repository;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="repository"></param>
        public DomainBase(IRepositoryBase<TEntity> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Busca a partir de um ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<TEntity> ObterPorId(int id)
        {
            try
            {
                return await _repository.FindById(id);
            }
            catch (Exception) { throw; }

        }

        /// <summary>
        /// Busca a partir de um ID.
        /// </summary>
        /// <param name="cod"></param>
        /// <returns></returns>
        public TEntity ObterPorCodigo(string cod)
        {
            try
            {
                return _repository.FindByCode(cod);
            }
            catch (Exception) { throw; }
        }

        /// <summary>
        /// Listar objetos
        /// </summary>
        /// <returns></returns>
        public IQueryable<TEntity> ListarTodos()
        {
            try
            {
                return _repository.FindAll();
            }
            catch (Exception) { throw; }
        }

        /// <summary>
        /// Excluir um objeto
        /// </summary>
        /// <param name="item"></param>
        public void Excluir(TEntity entidade)
        {
            try
            {
                _repository.Delete(entidade);
            }
            catch (Exception) { throw; }
        }

        /// <summary>
        /// Excluir um objeto
        /// </summary>
        /// <param name="item"></param>
        public virtual void Excluir(int entidadeId)
        {
            try
            {
                _repository.Delete(entidadeId);
            }
            catch (Exception) { throw; }
        }

        /// <summary>
        /// Inserir um objeto
        /// </summary>
        /// <param name="item"></param>
        public virtual async Task Cadastrar(TEntity entidade)
        {
            try
            {
                await _repository.Insert(entidade);
            }
            catch (Exception) { throw; }
        }

        /// <summary>
        /// Alterar um objeto
        /// </summary>
        /// <param name="item"></param>
        public virtual async Task Atualizar(TEntity entidade)
        {
            try
            {
                await _repository.Update(entidade);
            }
            catch (Exception) { throw; }
        }

        /// <summary>
        /// Lista os objetos na base baseado em uma expressão exemplo: c => c.ativo == true
        /// </summary>
        /// <returns></returns>
        public IQueryable<TEntity> ListarporExpressao(Expression<Func<TEntity, bool>> predicate)
        {
            return _repository.FindAllbyExpression(predicate);
        }
    }
}
