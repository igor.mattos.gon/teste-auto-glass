﻿using TesteAutoGlass.Dominio.Entidade.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TesteAutoGlass.Infraestrutura.RepositorioEF.Configuration
{
    public class ProdutoConfiguration : IEntityTypeConfiguration<Produto>
    {
        /// <summary>
        /// Método que mapeia os atributos da classe na tabela
        /// </summary>
        /// <param name="builder">Construtor do Mapeamento</param>
        public void Configure(EntityTypeBuilder<Produto> builder)
        {
            builder.ToTable("PRODUTO");

            builder.HasKey(m => m.IdProduto);

            builder.Property(m => m.IdProduto)
                   .HasColumnName("ID_PRODUTO")
                   .ValueGeneratedOnAdd()
                   .IsRequired();

            builder.Property(m => m.Descricao)
            .HasColumnName("DSC_PRODUTO")
            .HasMaxLength(700)
            .HasColumnType("VARCHAR")
            .IsRequired();

            builder.Property(m => m.Ativo)
            .HasColumnName("ATIVO")
            .HasColumnType("BIT")
            .IsRequired();

            builder.Property(m => m.DataFabricacao)
            .HasColumnName("DT_FABRICACAO")
            .HasColumnType("DATE")
            .IsRequired();

            builder.Property(m => m.DataValidade)
            .HasColumnName("DT_VALIDADE")
            .HasColumnType("DATE")
            .IsRequired();

            builder.Property(m => m.CodigoFornecedor)
            .HasColumnName("CD_FORNECEDOR")
            .HasMaxLength(50)
            .HasColumnType("VARCHAR")
            .IsRequired(false);

            builder.Property(m => m.DescricaoFornecedor)
            .HasColumnName("DSC_FORNECEDOR")
            .HasMaxLength(700)
            .HasColumnType("VARCHAR")
            .IsRequired(false);

            builder.Property(m => m.CNPJFornecedor)
            .HasColumnName("CNPJ_FORNECEDOR")
            .HasMaxLength(14)
            .HasColumnType("VARCHAR")
            .IsRequired(false);
        }
    }
}
