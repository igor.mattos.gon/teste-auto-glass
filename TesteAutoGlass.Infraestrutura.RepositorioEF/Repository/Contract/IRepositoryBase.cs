﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TesteAutoGlass.Infraestrutura.RepositorioEF.Repository.Contract
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        /// <summary>
        /// Busca a partir de um ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<TEntity> FindById(int id);

        /// <summary>
        /// Busca a partir de um ID.
        /// </summary>
        /// <param name="cod"></param>
        /// <returns></returns>
        TEntity FindByCode(string cod);

        /// <summary>
        /// Listar objetos
        /// </summary>
        /// <returns></returns>
        IQueryable<TEntity> FindAll();

        /// <summary>
        /// Excluir um objeto
        /// </summary>
        /// <param name="item"></param>
        void Delete(TEntity entidade);

        /// <summary>
        /// Excluir um objeto
        /// </summary>
        /// <param name="item"></param>
        void Delete(int entidadeId);

        /// <summary>
        /// Inserir um objeto
        /// </summary>
        /// <param name="item"></param>
        Task Insert(TEntity entidade);

        /// <summary>
        /// Alterar um objeto
        /// </summary>
        /// <param name="item"></param>
        Task Update(TEntity entidade);

        /// <summary>
        /// Lista os objetos na base baseado em uma expressão exemplo: c => c.ativo == true
        /// </summary>
        /// <returns></returns>
        IQueryable<TEntity> FindAllbyExpression(Expression<Func<TEntity, bool>> predicate);
    }
}
