﻿using TesteAutoGlass.Dominio.Entidade.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace TesteAutoGlass.Infraestrutura.RepositorioEF.Repository.Contract
{
    public interface IRepositoryProduto : IRepositoryBase<Produto>
    {
    }
}
