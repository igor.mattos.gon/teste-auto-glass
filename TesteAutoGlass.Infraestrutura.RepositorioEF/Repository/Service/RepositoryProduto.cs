﻿using TesteAutoGlass.Dominio.Entidade.Model;
using TesteAutoGlass.Infraestrutura.RepositorioEF.Context;
using TesteAutoGlass.Infraestrutura.RepositorioEF.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace TesteAutoGlass.Infraestrutura.RepositorioEF.Repository.Service
{
    public class RepositoryProduto : RepositoryBase<Produto>, IRepositoryProduto
    {
        public RepositoryProduto(DataContext dataContext)
            : base(dataContext)
        {
        }
    }
}
