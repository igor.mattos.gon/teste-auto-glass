﻿using System;
using TesteAutoGlass.Dominio.Entidade.Model;

namespace TesteAutoGlass.Apresentacao.WebApi.Model.Factory
{
    public static class ProdutoFactory
    {
        /// <summary>
        /// Obter instância de cadastro
        /// </summary>
        /// <param name="produtoCadastro">Objeto Cliente DTO</param>
        /// <returns>Instância Produto</returns>
        public static Produto GetInstanceCadastro(ProdutoModelCadastrar produtoCadastro)
        {
            if (produtoCadastro == null)
            {
                throw new System.Exception("Objeto nulo.");
            }

            Produto produto = new Produto(produtoCadastro.Descricao, produtoCadastro.DataFabricacao, produtoCadastro.DataValidade,
                                          produtoCadastro.CodigoFornecedor, produtoCadastro.DescricaoFornecedor, produtoCadastro.CNPJFornecedor);
            return produto;
        }

        /// <summary>
        /// Obter instância de alteração
        /// </summary>
        /// <param name="produtoAtualizacao">Objeto Cliente DTO</param>
        /// <returns>Instância Produto</returns>
        public static Produto GetInstanceAlteracao(ProdutoModelAtualizar produtoAtualizacao)
        {
            if (produtoAtualizacao == null)
            {
                throw new System.Exception("Objeto nulo.");
            }

            Produto produto = new Produto(produtoAtualizacao.IdProduto, produtoAtualizacao.Descricao, produtoAtualizacao.DataFabricacao, produtoAtualizacao.DataValidade, 
                                          produtoAtualizacao.CodigoFornecedor, produtoAtualizacao.DescricaoFornecedor, produtoAtualizacao.CNPJFornecedor);
            return produto;
        }
    }
}
