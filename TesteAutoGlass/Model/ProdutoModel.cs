﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace TesteAutoGlass.Apresentacao.WebApi.Model
{
    [DataContract]
    public class ProdutoModelCadastrar
    {
        [Required]
        [MinLength(3)]
        [MaxLength(700)]
        [DataMember(Name = "descricao")]
        public string Descricao { get; set; }

        [DataMember(Name = "ativo")]
        public bool Ativo { get; set; }

        [DataMember(Name = "dataFabricacao")]
        public DateTime DataFabricacao { get; set; }

        [DataMember(Name = "dataValidade")]
        public DateTime DataValidade { get; set; }

        [MaxLength(50)]
        [DataMember(Name = "codigoFornecedor")]
        public string CodigoFornecedor { get; set; }

        [MaxLength(700)]
        [DataMember(Name = "descricaoFornecedor")]
        public string DescricaoFornecedor { get; set; }
        
        [DataMember(Name = "cnpjFornecedor")]
        public string CNPJFornecedor { get; set; }
    }

    [DataContract]
    public class ProdutoModelAtualizar
    {
        [DataMember(Name = "idProduto")]
        public int IdProduto { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(700)]
        [DataMember(Name = "descricao")]
        public string Descricao { get; set; }        

        [DataMember(Name = "dataFabricacao")]
        public DateTime DataFabricacao { get; set; }

        [DataMember(Name = "dataValidade")]
        public DateTime DataValidade { get; set; }
        
        [MaxLength(50)]
        [DataMember(Name = "codigoFornecedor")]
        public string CodigoFornecedor { get; set; }

        [MaxLength(700)]
        [DataMember(Name = "descricaoFornecedor")]
        public string DescricaoFornecedor { get; set; }
        
        [DataMember(Name = "cnpjFornecedor")]
        public string CNPJFornecedor { get; set; }
    }

    [DataContract]
    public class ProdutoModelListar
    {
        [DataMember(Name = "idProduto")]
        public int IdProduto { get; set; }

        [DataMember(Name = "descricao")]
        public string Descricao { get; set; }

        [DataMember(Name = "ativo")]
        public bool Ativo { get; set; }

        [DataMember(Name = "dataFabricacao")]
        public DateTime DataFabricacao { get; set; }

        [DataMember(Name = "dataValidade")]
        public DateTime DataValidade { get; set; }

        [DataMember(Name = "codigoFornecedor")]
        public string CodigoFornecedor { get; set; }

        [DataMember(Name = "descricaoFornecedor")]
        public string DescricaoFornecedor { get; set; }

        [DataMember(Name = "cnpjFornecedor")]
        public string CNPJFornecedor { get; set; }
    }
}
