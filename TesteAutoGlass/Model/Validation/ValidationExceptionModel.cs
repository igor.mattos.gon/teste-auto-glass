﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace TesteAutoGlass.Apresentacao.WebApi.Model.Validation
{
    [DataContract]
    public class ValidationExceptionModel
    {
        public ValidationExceptionModel(string field, string message)
        {
            Field = field != string.Empty ? field : null;
            Message = message;
        }

        [DataMember(Name = "message")]
        public string Message { get; set; }

        [DataMember(Name = "field")]
        public string Field { get; set; }
    }

    [DataContract]
    public class ValidationResultModel
    {
        [DataMember(Name = "total")]
        public int Total { get; }

        [DataMember(Name = "content")]
        public List<ValidationExceptionModel> Content { get; }

        public ValidationResultModel(ModelStateDictionary modelState)
        {            
            Content = modelState.Keys
                    .SelectMany(key => modelState[key].Errors.Select(x => new ValidationExceptionModel(key, x.ErrorMessage)))
                    .ToList();
            Total = Content != null ? ((Content.GetType().GetInterface(nameof(IList)) != null) ? (Content as IList).Count : 1) : 0;
        }
    }
}
