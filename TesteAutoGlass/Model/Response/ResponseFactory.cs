﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace TesteAutoGlass.Apresentacao.WebApi.Model.Response
{
    /// <summary>
    /// Classe responsável pelo tratamento da resposta devolvida ao cliente
    /// </summary>
    public static class ResponseFactory<T>
    {
        /// <summary>
        /// Método responsável por tratar a resposta ao cliente
        /// </summary>        
        /// <param name="content">Valor da Resposta</param>
        /// <returns>Objeto Response</returns>
        public static ResponseModel<T> GerarResponse(T content)
        {
            int totalItens = 1;

            //REST
            if (content.GetType().GetInterface(nameof(IList)) != null)
            {
                totalItens = (content as IList).Count;
            }

            //GRAPHQL
            if (content.GetType().GetInterface(nameof(IDictionary<string, object>)) != null)
            {
                totalItens = (content as IDictionary<string, object>).ElementAt(0).Value as IDictionary<string, object> != null ?
                              ((IDictionary<string, object>)(content as IDictionary<string, object>).ElementAt(0).Value).Count :
                              ((IList)(content as IDictionary<string, object>).ElementAt(0).Value).Count;
            }

            ResponseModel<T> response = new ResponseModel<T>()
            {
                Total = totalItens,
                Content = content
            };

            return response;
        }
    }

    /// <summary>
    /// Classe de modelo usada para a resposta
    /// </summary>
    [DataContract]
    public class ResponseModel<T>
    {
        [DataMember(Name = "total")]
        public int Total { get; set; }

        [DataMember(Name = "content")]
        public T Content { get; set; }
    }
}

