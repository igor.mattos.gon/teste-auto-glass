using TesteAutoGlass.Infraestrutura.RepositorioEF.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TesteAutoGlass.Dominio.Negocio.Contract;
using TesteAutoGlass.Dominio.Negocio.Service;
using TesteAutoGlass.Infraestrutura.RepositorioEF.Repository.Contract;
using TesteAutoGlass.Infraestrutura.RepositorioEF.Repository.Service;
using TesteAutoGlass.Apresentacao.WebApi.Model;
using TesteAutoGlass.Dominio.Entidade.Model;
using AutoMapper;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using TesteAutoGlass.Apresentacao.WebApi.Model.Exception;
using TesteAutoGlass.Apresentacao.WebApi.Filters;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Model;
using TesteAutoGlass.Infraestrutura.Utilidades.Contract;

namespace TesteAutoGlass
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Adicionar formata��o de data
            services.AddControllers().AddNewtonsoftJson
                (options =>
                {
                    options.SerializerSettings.DateFormatString = "yyyy-MM-dd hh:mm:ss";
                });

            //Configurar a string de conex�o
            services.AddDbContext<DataContext>(options =>
                                               options
                                               .UseLoggerFactory(LoggerFactory.Create(builder => builder.AddDebug()))
                                               .UseLazyLoadingProxies()
                                               .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking)
                                               .UseSqlServer(Configuration.GetConnectionString("DB")));

            //Inje��o de Depend�ncia do dom�nio            
            services.AddScoped(typeof(IDomainProduto), typeof(DomainProduto));

            //Inje��o de Depend�ncia do reposit�rio
            services.AddScoped(typeof(IRepositoryProduto), typeof(RepositoryProduto));

            //Inje��o de Depend�ncia de utilidades
            services.AddScoped(typeof(IValidation), typeof(Infraestrutura.Utilidades.Service.Validation));

            //Auto Mapper
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {                
                cfg.CreateMap<Produto, ProdutoModelListar>();
            });

            IMapper mapper = config.CreateMapper();
            services.AddSingleton(mapper);

            //Adicionar o Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "TesteAutoGlass", Version = "v1" });
            });

            //Adi��o da Rota
            services.AddRouting(options => options.LowercaseUrls = true);

            //Adicionar o filtro de valida��o do modelo
            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(ValidateModelAttribute));
            })
            .ConfigureApiBehaviorOptions(options =>
            {
                options.SuppressMapClientErrors = true;
                options.SuppressModelStateInvalidFilter = true;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "TesteAutoGlass v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseCors(c => c.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            //Configurar o handler para gera��o da exce��o que n�o for status code 400
            app.UseExceptionHandler(c => c.Run(async context =>
            {
                IExceptionHandlerPathFeature exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();
                Exception exception = exceptionHandlerPathFeature.Error;

                await context.Response.WriteAsJsonAsync(ExceptionFactory.GerarExcecao(exception));
            }));
        }
    }
}
