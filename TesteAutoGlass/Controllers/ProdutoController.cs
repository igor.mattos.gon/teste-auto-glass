﻿using TesteAutoGlass.Apresentacao.WebApi.Model;
using TesteAutoGlass.Dominio.Entidade.Model;
using TesteAutoGlass.Dominio.Negocio.Contract;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TesteAutoGlass.Apresentacao.WebApi.Model.Exception;
using TesteAutoGlass.Apresentacao.WebApi.Model.Response;
using TesteAutoGlass.Apresentacao.WebApi.Filters;
using TesteAutoGlass.Apresentacao.WebApi.Model.Factory;
using TesteAutoGlass.Infraestrutura.Utilidades.Message;

namespace TesteAutoGlass.Apresentacao.WebApi.Controllers
{
    [Route("api/")]
    [ApiController]
    [ValidateModel]
    public class ProdutoController : ControllerBase
    {
        private readonly IDomainProduto _domainProduto;
        private IMapper _mapper;

        public ProdutoController(IDomainProduto domainProduto, IMapper mapper)
        {
            _domainProduto = domainProduto;
            _mapper = mapper;
        }

        /// <summary>
        /// API de cadastro
        /// </summary>
        /// <param name="produto">Objeto produto</param>
        /// <returns>Produto cadastrado</returns>
        [HttpPost("produto")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]        
        public async Task<ActionResult> Post(ProdutoModelCadastrar produto)
        {
            try
            {
                Produto produtoCadastro = ProdutoFactory.GetInstanceCadastro(produto);
                await _domainProduto.Cadastrar(produtoCadastro);

                return CreatedAtAction(nameof(Post), produtoCadastro);
            }

            catch (Exception ex)
            {
                return BadRequest(ResponseFactory<ExceptionModel>.GerarResponse(ExceptionFactory.GerarExcecao(ex)));
            }
        }

        /// <summary>
        /// API de atualização
        /// </summary>
        /// <param name="produto">Objeto produto</param>
        /// <returns>Produto cadastrado</returns>
        [HttpPut("produto")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]        
        public async Task<ActionResult> Put(ProdutoModelAtualizar produto)
        {
            try
            {
                Produto produtoAtualizacao = ProdutoFactory.GetInstanceAlteracao(produto);

                await _domainProduto.Atualizar(produtoAtualizacao);
                return Ok(ResponseFactory<string>.GerarResponse(AppMessage.PRODUTO_ATUALIZADO));
            }

            catch (Exception ex)
            {
                return BadRequest(ResponseFactory<ExceptionModel>.GerarResponse(ExceptionFactory.GerarExcecao(ex)));
            }
        }

        /// <summary>
        /// API de atualização
        /// </summary>
        /// <param name="produto">Objeto produto</param>
        /// <returns>Produto cadastrado</returns>
        [HttpDelete("produto/{id}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult Delete(int id)
        {
            try
            {
                _domainProduto.Excluir(id);
                return Ok(ResponseFactory<string>.GerarResponse(AppMessage.PRODUTO_EXCLUIDO));
            }

            catch (Exception ex)
            {
                return BadRequest(ResponseFactory<ExceptionModel>.GerarResponse(ExceptionFactory.GerarExcecao(ex)));
            }
        }


        /// <summary>
        /// Metodo de listagem
        /// </summary>
        /// <param name="skip">Total de itens a serem pulados/ignorados da lista</param>
        /// <param name="take">Total de itens a serem retornados</param>
        /// <returns>Lista de produtos</returns>
        [HttpGet("produtos")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]       
        public async Task<ActionResult> Get(int skip, int take)
        {
            try
            {
                IEnumerable<ProdutoModelListar> produtos = _mapper.Map<IEnumerable<Produto>, IEnumerable<ProdutoModelListar>>(await _domainProduto.ListarporExpressao(f => f.Ativo)
                                                                                                                                                  .Skip(skip)
                                                                                                                                                  .Take(take)
                                                                                                                                                  .OrderByDescending(f => f.DataValidade)
                                                                                                                                                  .ToListAsync());
                if (produtos.Count() == 0)
                {
                    return NotFound(ResponseFactory<string>.GerarResponse(AppMessage.PRODUTO_NAO_ENCONTRADO));
                }

                return Ok(ResponseFactory<IEnumerable<ProdutoModelListar>>.GerarResponse(produtos));
            }

            catch (Exception ex)
            {
                return BadRequest(ResponseFactory<ExceptionModel>.GerarResponse(ExceptionFactory.GerarExcecao(ex)));
            }
        }

        /// <summary>
        /// Metodo de listagem
        /// </summary>
        /// <param name="id">Id do Produto</param>        
        /// <returns>Produto cadastrado</returns>
        [HttpGet("produto/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]        
        public async Task<ActionResult> GetById(int id)
        {
            try
            {
                ProdutoModelListar produto = _mapper.Map<Produto, ProdutoModelListar>(await _domainProduto.ListarporExpressao(f => f.IdProduto == id && f.Ativo).FirstOrDefaultAsync());

                if (produto == null)
                {
                    return NotFound(ResponseFactory<string>.GerarResponse(AppMessage.PRODUTO_NAO_ENCONTRADO));
                }

                return Ok(ResponseFactory<ProdutoModelListar>.GerarResponse(produto));
            }

            catch (Exception ex)
            {
                return BadRequest(ResponseFactory<ExceptionModel>.GerarResponse(ExceptionFactory.GerarExcecao(ex)));
            }
        }
    }
}
