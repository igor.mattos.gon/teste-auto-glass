﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TesteAutoGlass.Dominio.Entidade.Model
{
    /// <summary>
    /// Classe Produto
    /// </summary>
    public class Produto
    {
        //Usado pelo Entity Framework
        protected Produto()
        {
        }

        /// <summary>
        /// Contrutor de Cadastro
        /// </summary>
        /// <param name="descricao">Descrição do Produto</param>        
        /// <param name="dataFabricao">Data de Fabricação</param>
        /// <param name="dataValidade">Data de Validade</param>
        /// <param name="codigoFornecedor">Codigo do Fornecedor</param>
        /// <param name="descricaoFornecedor">Descrição do Fornecedor</param>
        /// <param name="cnpjFornecedor">CNPJ do Fornecedor</param>
        public Produto(string descricao, DateTime dataFabricao, DateTime dataValidade, string codigoFornecedor, string descricaoFornecedor,
                       string cnpjFornecedor)
        {
            Descricao = descricao;
            Ativo = true;
            DataFabricacao = dataFabricao;
            DataValidade = dataValidade;
            CodigoFornecedor = codigoFornecedor;
            DescricaoFornecedor = descricaoFornecedor;
            CNPJFornecedor = cnpjFornecedor;
        }

        /// <summary>
        /// Contrutor de Alteração
        /// </summary>
        /// <param name="idProduto">Id do Produto</param>
        /// <param name="descricao">Descrição do Produto</param>
        /// <param name="ativo">Ativo ou Inativo</param>
        /// <param name="dataFabricao">Data de Fabricação</param>
        /// <param name="dataValidade">Data de Validade</param>
        /// <param name="codigoFornecedor">Codigo do Fornecedor</param>
        /// <param name="descricaoFornecedor">Descrição do Fornecedor</param>
        /// <param name="cnpjFornecedor">CNPJ do Fornecedor</param>
        public Produto(int idProduto, string descricao, DateTime dataFabricao, DateTime dataValidade, string codigoFornecedor, string descricaoFornecedor,
                       string cnpjFornecedor)
        {
            IdProduto = idProduto;
            Descricao = descricao;            
            DataFabricacao = dataFabricao;
            DataValidade = dataValidade;
            CodigoFornecedor = codigoFornecedor;
            DescricaoFornecedor = descricaoFornecedor;
            CNPJFornecedor = cnpjFornecedor;

            Ativo = true;
        }

        public int IdProduto { get; private set; }

        public string Descricao { get; private set; }

        public bool Ativo { get; private set; }

        public DateTime DataFabricacao { get; private set; }

        public DateTime DataValidade { get; private set; }

        public string CodigoFornecedor { get; private set; }

        public string DescricaoFornecedor { get; private set; }

        public string CNPJFornecedor { get; private set; }

        #region Métodos        

        public void InativarProduto()
        {
            Ativo = false;
        }

        #endregion
    }
}
